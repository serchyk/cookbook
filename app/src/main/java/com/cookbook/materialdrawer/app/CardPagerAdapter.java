package com.cookbook.materialdrawer.app;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cookbook.materialdrawer.Drawer;
import com.cookbook.weed.app.R;

import java.util.ArrayList;
import java.util.List;

public class CardPagerAdapter extends PagerAdapter implements CardAdapter {

    private List<CardView> mViews;
    private List<CardItem> mData;
    private float mBaseElevation;
    public Button bNext;


    public CardPagerAdapter() {
        mData = new ArrayList<>();
        mViews = new ArrayList<>();
    }

    public void addCardItem(CardItem item) {
        mViews.add(null);
        mData.add(item);
    }

    public float getBaseElevation() {
        return mBaseElevation;
    }

    @Override
    public CardView getCardViewAt(int position) {
        return mViews.get(position);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.adapter, container, false);
        container.addView(view);
        bind(mData.get(position), view);
        CardView cardView = (CardView) view.findViewById(R.id.cardView);
        //CrossfadeDrawerLayoutActvitiy.buttonID = position;

        if (mBaseElevation == 0) {
            mBaseElevation = cardView.getCardElevation();
        }

        cardView.setMaxCardElevation(mBaseElevation * MAX_ELEVATION_FACTOR);
        mViews.set(position, cardView);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mViews.set(position, null);
    }

    public void bind(final CardItem item, final View view) {

        final ImageView imageView = (ImageView) view.findViewById(R.id.imageView3);
        final TextView titleTextView = (TextView) view.findViewById(R.id.titleTextView);
        TextView contentTextView = (TextView) view.findViewById(R.id.contentTextView);
        final ImageView zakladka = (ImageView) view.findViewById(R.id.zakladka);
        bNext = (Button) view.findViewById(R.id.button_next);
        imageView.setImageDrawable(item.getDraw());
        titleTextView.setText(item.getTitle());
        contentTextView.setText(item.getText());
        zakladka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.zakladka:
                        Toast.makeText(item.getPager(),item.getTitle() + " добавлено в закладки",Toast.LENGTH_SHORT).show();
                        break;

                }
            }
        });
        bNext.setText(R.string.open);

        bNext.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {

                CrossfadeDrawerLayoutActvitiy.articles.clear();
                CrossfadeDrawerLayoutActvitiy.articlesImg.clear();

                if(item.getPager().openApp == 1){
                    item.getPager().buttonID = item.getPager().openAppRecipeID;
                    item.getPager().loadOpenJsonRecipes();
                }else {

                    item.getPager().buttonID = item.getPager().newViewPager.getCurrentItem();
                    item.getPager().loadJsonRecipes();
                }

                Intent intent = new Intent(item.getPager(),Main2Activity.class);
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Pair[] pairs = new Pair[1];
                    pairs[0] = new Pair<View, String>(imageView, "imageTransition");

                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(item.getPager(), pairs);
                    //item.getPager().startActivity(intent);
                    Main2Activity.cDLA = item.getPager();
                    intent.putExtra("title", titleTextView.getText().toString());
                    System.out.print("FUCK = " + 1);
                    intent.putExtra("navP", item.getPager().navPosition);
                    item.getPager().startActivity(intent, options.toBundle());


                }else {
                    intent.putExtra("title", titleTextView.getText().toString());
                    item.getPager().startActivity(intent);
                }


            }
        });

    }


}
