package com.cookbook.materialdrawer.app;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cookbook.materialdrawer.app.fragment.VerticalStepperAdapterDemoFragment;
import com.cookbook.weed.app.R;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.cookbook.materialdrawer.Drawer;
import com.cookbook.materialdrawer.DrawerBuilder;
import com.cookbook.materialdrawer.MiniDrawer;
import com.cookbook.materialdrawer.interfaces.ICrossfader;
import com.cookbook.materialdrawer.model.PrimaryDrawerItem;
import com.cookbook.materialdrawer.model.interfaces.IDrawerItem;
import com.cookbook.materialdrawer.util.DrawerUIUtils;
import com.mikepenz.materialize.util.UIUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

import static android.content.Intent.ACTION_BOOT_COMPLETED;

public class CrossfadeDrawerLayoutActvitiy extends AppCompatActivity {

    public static final String APP_PREFERENCES = "array_set";
    public static final String APP_PREFERENCES_ARRAY = "array";
    public static int openApp;
    public static int openAppRecipeID;
    public static String arraySweets = "Origami";
//    public static String arrayDrinks = "Drinks";
//    public static String arraySoups = "Soups";
//    public static String arraySalads = "Salads";
//    public static String arraySecondCourse = "Second_dish";
//    public static String arraySnacks = "Snacks";
//    public static String arrayPastry = "Pastry";
    public FrameLayout frameLayout;
    public  ViewPager newViewPager;
    private Drawer result = null;
    private CrossfadeDrawerLayout crossfadeDrawerLayout = null;

    public int navPosition;

    public ArrayList<PrimaryDrawerItem> primaryDrawerItems = new ArrayList<>();

    public ViewPager mViewPager;
    public Context contexts;
    public CardPagerAdapter mCardAdapter;
    public ShadowTransformer mCardShadowTransformer;
    private CardFragmentPagerAdapter mFragmentCardAdapter;
    private ShadowTransformer mFragmentCardShadowTransformer;
    public String recipe;
    public String arrayC;
    SharedPreferences mSettings;
    public LinearLayout linearLayout;
    public Intent intent;
    public static ArrayList<String> articles = new ArrayList<String>();
    public static ArrayList<Drawable> articlesImg = new ArrayList<Drawable>();
    public static String prefixes;
    public static Integer buttonID;
    public String prefixLanguage;
    public ArrayList<String> locals= new ArrayList<>();
    public static int load_langue;

    VerticalStepperAdapterDemoFragment vSADF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_BOOT_COMPLETED);

        String local_lang = Locale.getDefault().getLanguage().toString();
        loadJsonLocal();

        if(load_langue == 0){
            prefixLanguage = "en";
        }

        for(int j = 0; j < locals.size(); j++){

            // Log.e("Test: ", local_lang);
            if(local_lang.equals(locals.toArray()[j].toString())) {
                prefixLanguage = locals.toArray()[j].toString();
                load_langue = 1;
            }

        }

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        intent = new Intent(this,Main2Activity.class);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        contexts = this;
        //set the back arrow in the toolbar
        getSupportActionBar().setTitle(R.string.drawer_item_crossfade_drawer_layout_drawerRandom);


        //
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);

        loadJsonNav();
        linearLayout = (LinearLayout) findViewById(R.id.linear);
        mViewPager = (ViewPager) findViewById(R.id.viewPager);

        result = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withHasStableIds(true)
                .withDrawerLayout(R.layout.crossfade_drawer)
                .withDrawerWidthDp(72)
                .withGenerateMiniDrawer(true)
                .withSavedInstance(savedInstanceState)
                .withShowDrawerOnFirstLaunch(true)
                .build();
        for(int k = 0; k < primaryDrawerItems.toArray().length; k++){
            result.addItem(primaryDrawerItems.get(k));
        }
        navPosition = result.getCurrentSelectedPosition();
        openApp = 1;
        if (mSettings.contains(APP_PREFERENCES_ARRAY)) {
            arrayC = (mSettings.getString(APP_PREFERENCES_ARRAY, ""));
        }
        crossfadeDrawerLayout = (CrossfadeDrawerLayout) result.getDrawerLayout();


        if (arrayC != null) {
            loadJson();
            mFragmentCardAdapter = new CardFragmentPagerAdapter(getSupportFragmentManager(),
                    dpToPixels(2, this));

            mCardShadowTransformer = new ShadowTransformer(mViewPager, mCardAdapter);
            mFragmentCardShadowTransformer = new ShadowTransformer(mViewPager, mFragmentCardAdapter);

            mViewPager.setAdapter(mCardAdapter);
            mViewPager.setPageTransformer(false, mCardShadowTransformer);
            mViewPager.setOffscreenPageLimit(3);

            mCardShadowTransformer.enableScaling(true);
        }

        crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
        //add second view (which is the miniDrawer)
        final MiniDrawer miniResult = result.getMiniDrawer();
        //build the view for the MiniDrawer
        View view = miniResult.build(this);
        //set the background of the MiniDrawer as this would be transparent
        view.setBackgroundColor(UIUtils.getThemeColorFromAttrOrRes(this, com.cookbook.materialdrawer.R.attr.material_drawer_background, com.cookbook.materialdrawer.R.color.material_drawer_background));
        //we do not have the MiniDrawer view during CrossfadeDrawerLayout creation so we will add it here
        crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        //define the crossfader to be used with the miniDrawer. This is required to be able to automatically toggle open / close
        miniResult.withCrossFader(new ICrossfader() {
            @Override
            public void crossfade() {

                boolean isFaded = isCrossfaded();
                crossfadeDrawerLayout.crossfade(400);

                //only close the drawer if we were already faded and want to close it now
                if (isFaded) {
                    result.getDrawerLayout().closeDrawer(GravityCompat.START);
                }
            }

            @Override
            public boolean isCrossfaded() {
                return crossfadeDrawerLayout.isCrossfaded();
            }
        });
        openActivity();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //add the values which need to be saved from the drawer to the bundle
        outState = result.saveInstanceState(outState);
        //add the values which need to be saved from the accountHeader to the bundle
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        //handle the back press :D close the drawer first and if the drawer is closed close the activity
        if (result != null && result.isDrawerOpen()) {
            result.closeDrawer();
        } else {

            super.onBackPressed();
        }

    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }


    //load local\\
    public void loadJsonLocal() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAssetNav());
            JSONArray m_jArry = obj.getJSONArray("local");


            for (int j = 0; j < m_jArry.length(); j++) {
                JSONObject joR_inside = m_jArry.getJSONObject(j);
                final String name = joR_inside.getString("local_string");
                locals.add(name);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //load navigator\\
    public void loadJsonNav() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAssetNav());
            JSONArray m_jArry = obj.getJSONArray("navigator");


                for (int j = 0; j < m_jArry.length(); j++) {
                    JSONObject joR_inside = m_jArry.getJSONObject(j);
                    final String name = joR_inside.getString("NameCategory");
                    final String nameArray = joR_inside.getString("arrayObjectsName");
                    String pref = joR_inside.getString("img_prefix");
                    Drawable d = Drawable.createFromStream(getApplicationContext().getAssets().open("Img_Categories/"+pref+".png"), null);
                    primaryDrawerItems.add(new PrimaryDrawerItem().withName(name).withIcon(d).withIdentifier(j+1).withSelectable(true));
                    if(j == 0) {
                        primaryDrawerItems.get(j).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                            @Override
                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                getSupportActionBar().setTitle(name);
                                restartRandomActivity();
                                return false;
                            }
                        });
                    }
                    else {
                        primaryDrawerItems.get(j).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                            @Override
                            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                                arrayC = nameArray;
                                getSupportActionBar().setTitle(name);
                                restartActivity();
                                return false;
                            }
                        });
                    }
                }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAssetNav() {
        String json = null;
        try {
            InputStream is = CrossfadeDrawerLayoutActvitiy.this.getAssets().open("nav.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    //********************************************************************\\

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = CrossfadeDrawerLayoutActvitiy.this.getAssets().open("book.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String loadJSONFromAssetRecipe() {
        String json = null;
        try {
            InputStream is = CrossfadeDrawerLayoutActvitiy.this.getAssets().open(recipe+"/"+recipe+".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void loadJson() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("Origami");


            for (int i = 0; i < m_jArry.length(); i++) {

                JSONObject jo_inside = m_jArry.getJSONObject(i);
                JSONArray m_jArryR = jo_inside.getJSONArray("files");
                for (int j = 0; j < m_jArryR.length(); j++) {
                    JSONObject joR_inside = m_jArryR.getJSONObject(j);
                    recipe = joR_inside.getString("file_recipe");
                    vSADF = new VerticalStepperAdapterDemoFragment();

                    JSONObject objR = new JSONObject(loadJSONFromAssetRecipe());

                    String name = objR.getString("name_"+prefixLanguage);
                    String description = objR.getString("description_" + prefixLanguage);
                    String pref = objR.getString("img_prefix");
                    Drawable d = Drawable.createFromStream(getApplicationContext().getAssets().open(pref+"/"+pref+".webp"), null);


                    JSONArray arrayR = objR.getJSONArray("stages");
                    for (int ji = 0; ji < arrayR.length(); ji++) {
                        JSONObject jR = arrayR.getJSONObject(ji);
                        articles.add(jR.getString("description_" + prefixLanguage));
                    }
                    mCardAdapter.addCardItem(new CardItem(name, description,this,d));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


//Load Random Array with recipe
    public void loadJsonOpen() {
        String[] arrays = new String[]{
                "Origami"
        };
        Random random = new Random();
//        arrayC = arrays[random.nextInt(5)];
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("Origami");


            for (int i = 0; i < m_jArry.length(); i++) {

                JSONObject jo_inside = m_jArry.getJSONObject(i);
                JSONArray m_jArryR = jo_inside.getJSONArray("files");
                openAppRecipeID = random.nextInt(m_jArryR.length());
                    JSONObject joR_inside = m_jArryR.getJSONObject(openAppRecipeID);
                    recipe = joR_inside.getString("file_recipe");
                    vSADF = new VerticalStepperAdapterDemoFragment();

                    JSONObject objR = new JSONObject(loadJSONFromAssetRecipe());

                    String name = objR.getString("name_"+prefixLanguage);
                    String description = objR.getString("description_" + prefixLanguage);
                    String pref = objR.getString("img_prefix");
                    Drawable d = Drawable.createFromStream(getApplicationContext().getAssets().open(pref+"/"+pref+".webp"), null);


                    JSONArray arrayR = objR.getJSONArray("stages");
                    for (int ji = 0; ji < arrayR.length(); ji++) {
                        JSONObject jR = arrayR.getJSONObject(ji);
                        articles.add(jR.getString("description_" + prefixLanguage));
                    }
                    mCardAdapter.addCardItem(new CardItem(name, description,this,d));
                }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
//Random recipe
    public void loadOpenJsonRecipes() {

        try {

            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("Origami");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                JSONArray m_jArryR = jo_inside.getJSONArray("files");

                JSONObject joR_inside = m_jArryR.getJSONObject(openAppRecipeID);

                recipe = joR_inside.getString("file_recipe");
                vSADF = new VerticalStepperAdapterDemoFragment();

                JSONObject objR = new JSONObject(loadJSONFromAssetRecipe());

                prefixes = objR.getString("img_prefix");

                JSONArray arrayR = objR.getJSONArray("stages");
                for (int ji = 0; ji < arrayR.length(); ji++) {

                    JSONObject jR = arrayR.getJSONObject(ji);
                    Drawable d = Drawable.createFromStream(getApplicationContext().getAssets().open(prefixes+"/"+ji+".webp"), null);
                    articles.add(jR.getString("description_"+prefixLanguage));
                    articlesImg.add(d);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadJsonRecipes() {

        try {

            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("Origami");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                JSONArray m_jArryR = jo_inside.getJSONArray("files");

                    JSONObject joR_inside = m_jArryR.getJSONObject(newViewPager.getCurrentItem());

                    recipe = joR_inside.getString("file_recipe");
                    vSADF = new VerticalStepperAdapterDemoFragment();

                    JSONObject objR = new JSONObject(loadJSONFromAssetRecipe());

                    prefixes = objR.getString("img_prefix");

                    JSONArray arrayR = objR.getJSONArray("stages");
                    for (int ji = 0; ji < arrayR.length(); ji++) {

                        JSONObject jR = arrayR.getJSONObject(ji);
                        Drawable d = Drawable.createFromStream(getApplicationContext().getAssets().open(prefixes+"/"+ji+".webp"), null);
                        articles.add(jR.getString("description_"+prefixLanguage));
                         articlesImg.add(d);
                    }
                }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //MATCH
    public void openActivity(){
        linearLayout.removeAllViews();
        LinearLayout container = (LinearLayout) findViewById(R.id.linear);
        newViewPager = new ViewPager(this);
        container.addView(newViewPager);

        mCardAdapter = new CardPagerAdapter();

        loadJsonOpen();
        result.closeDrawer();
        newViewPager.setAdapter(mCardAdapter);
        newViewPager.setPageTransformer(false, mCardShadowTransformer);
        newViewPager.setOffscreenPageLimit(3);
    }
    public void restartActivity() {

        openApp = 0;
        View myView = findViewById(R.id.frame_container);

        // get the center for the clipping circle
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            int cx = (myView.getLeft() + 0) / 2;
            int cy = (0 + myView.getBottom()) / 2;

            // get the final radius for the clipping circle
            int dx = Math.max(cx, myView.getWidth() - cx);
            int dy = Math.max(cy, myView.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);

            // Android native animator
            Animator animator = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(500);
            animator.start();
        }
        result.closeDrawer();
        linearLayout.removeAllViews();
        LinearLayout container = (LinearLayout) findViewById(R.id.linear);
        newViewPager = new ViewPager(this);

        container.addView(newViewPager);

        mCardAdapter = new CardPagerAdapter();

        loadJson();
        result.closeDrawer();
        newViewPager.setAdapter(mCardAdapter);
        newViewPager.setPageTransformer(false, mCardShadowTransformer);
        newViewPager.setOffscreenPageLimit(3);


    }
    public void restartRandomActivity() {

        openApp = 1;
        View myView = findViewById(R.id.frame_container);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // get the center for the clipping circle
            int cx = (myView.getLeft() + 0) / 2;
            int cy = (0 + myView.getBottom()) / 2;

            // get the final radius for the clipping circle
            int dx = Math.max(cx, myView.getWidth() - cx);
            int dy = Math.max(cy, myView.getHeight() - cy);
            float finalRadius = (float) Math.hypot(dx, dy);

            // Android native animator
            Animator animator = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(500);
            animator.start();
        }
        linearLayout.removeAllViews();
        LinearLayout container = (LinearLayout) findViewById(R.id.linear);
        newViewPager = new ViewPager(this);

        container.addView(newViewPager);

        mCardAdapter = new CardPagerAdapter();

        loadJsonOpen();
        result.closeDrawer();
        newViewPager.setAdapter(mCardAdapter);
        newViewPager.setPageTransformer(false, mCardShadowTransformer);
        newViewPager.setOffscreenPageLimit(3);


    }
}