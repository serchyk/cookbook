package com.cookbook.materialdrawer.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cookbook.materialdrawer.IStepperAdapter;
import com.cookbook.materialdrawer.VerticalStepperItemView;
import com.cookbook.materialdrawer.VerticalStepperView;
import com.cookbook.materialdrawer.app.CrossfadeDrawerLayoutActvitiy;
import com.cookbook.materialdrawer.app.Main2Activity;
import com.cookbook.weed.app.R;

import java.util.ArrayList;

public class VerticalStepperAdapterDemoFragment extends Fragment implements IStepperAdapter {

	public ArrayList<String> descriptionRu = CrossfadeDrawerLayoutActvitiy.articles;
	public ArrayList<Drawable> stepArray = CrossfadeDrawerLayoutActvitiy.articlesImg;
	//descriptionRu =
	private VerticalStepperView mVerticalStepperView;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_vertical_stepper_adapter, parent, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		mVerticalStepperView = view.findViewById(R.id.vertical_stepper_view);
		mVerticalStepperView.setStepperAdapter(this);
	}

	@Override
	public @NonNull CharSequence getTitle(int index) {
		return "Step " + index;
	}

	@Override
	public @Nullable CharSequence getSummary(int index) {
		switch (index) {
			default:
				return null;
		}
	}

	@Override
	public int size() {
		return descriptionRu.size();
	}
	@Override
	public View onCreateCustomView(final int index, final Context context, VerticalStepperItemView parent) {
		//descriptionRu = CrossfadeDrawerLayoutActvitiy.articles;
		View inflateView = LayoutInflater.from(context).inflate(R.layout.vertical_stepper_sample_item, parent, false);
		TextView contentView = inflateView.findViewById(R.id.item_content);
		ImageView imageView = inflateView.findViewById(R.id.imageView2);

		imageView.setImageDrawable((Drawable) stepArray.toArray()[index]);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			imageView.setTransitionName("imageTransition");
		}


		contentView.setText(descriptionRu.toArray()[index].toString());

		final Button nextButton = inflateView.findViewById(R.id.button_next);
		nextButton.setText(R.string.ok);
		if(index == descriptionRu.size()-1){
			nextButton.setText(R.string.great);
//			nextButton.setOnClickListener(new View.OnClickListener() {
//				@Override
//				public void onClick(View view) {
//
//				}
//			});
		}

		nextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!mVerticalStepperView.nextStep()) {
//					mVerticalStepperView.setErrorText(0, mVerticalStepperView.getErrorText(0) == null ? "Test error" : null);
					getActivity().onBackPressed();
				}
			}
		});
		Button prevButton = inflateView.findViewById(R.id.button_prev);
		prevButton.setText(R.string.prev);
		if(index == 0){
			prevButton.setVisibility(View.INVISIBLE);
		}
		inflateView.findViewById(R.id.button_prev).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
					mVerticalStepperView.prevStep();

			}
		});
		return inflateView;
	}

	@Override
	public void onShow(int index) {

	}

//	@Override
//	public void onHide(int index) {
//		onShow(index);
//	}

}
