package com.cookbook.materialdrawer.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cookbook.materialdrawer.Drawer;
import com.cookbook.materialdrawer.DrawerBuilder;
import com.cookbook.materialdrawer.MiniDrawer;
import com.cookbook.materialdrawer.app.fragment.VerticalStepperAdapterDemoFragment;
import com.cookbook.materialdrawer.interfaces.ICrossfader;
import com.cookbook.materialdrawer.model.PrimaryDrawerItem;
import com.cookbook.materialdrawer.model.interfaces.IDrawerItem;
import com.cookbook.materialdrawer.util.DrawerUIUtils;
import com.cookbook.weed.app.R;
import com.mikepenz.crossfadedrawerlayout.view.CrossfadeDrawerLayout;
import com.mikepenz.google_material_typeface_library.GoogleMaterial;
import com.mikepenz.materialize.util.UIUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {

    public  static View view;
    private RelativeLayout mDrawerLayout;
    private Drawer result = null;
    public static final String APP_PREFERENCES = "array_set";
    ArrayList<PrimaryDrawerItem> primaryDrawerItems = new ArrayList<>();
    public static CrossfadeDrawerLayoutActvitiy cDLA;
    SharedPreferences mSettings;



    public Fragment mVerticalStepperAdapterDemoFragment = new VerticalStepperAdapterDemoFragment();
    private CrossfadeDrawerLayout crossfadeDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        //ArrayList<String> arrayFromIntent = (ArrayList<String>) getIntent().getSerializableExtra("list");
        //Toast.makeText(this,arrayFromIntent.toArray()[0].toString(),Toast.LENGTH_LONG).show();
        mDrawerLayout = findViewById(R.id.drawer_layout);
        Intent intent = getIntent();
        String actionBar = intent.getStringExtra("title");
        //Toast.makeText(this, testes,Toast.LENGTH_SHORT).show();


        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            loadJsonNav();
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(actionBar);
            //Create the drawer
            result = new DrawerBuilder().withActivity(this).withToolbar(toolbar).withHasStableIds(true).withDrawerLayout(R.layout.crossfade_drawer).withDrawerWidthDp(72).withGenerateMiniDrawer(true)
                    .withSavedInstance(savedInstanceState).withShowDrawerOnFirstLaunch(true).build();
            for(int k = 0; k < primaryDrawerItems.toArray().length; k++){
                result.addItem(primaryDrawerItems.get(k));
            }
            crossfadeDrawerLayout = (CrossfadeDrawerLayout) result.getDrawerLayout();

           // result.setSelection(intent.getExtras("navP"));


            crossfadeDrawerLayout.setMaxWidthPx(DrawerUIUtils.getOptimalDrawerWidth(this));
            //add second view (which is the miniDrawer)
            final MiniDrawer miniResult = result.getMiniDrawer();
            //build the view for the MiniDrawer
            View view = miniResult.build(this);
            //set the background of the MiniDrawer as this would be transparent
            view.setBackgroundColor(UIUtils.getThemeColorFromAttrOrRes(this, com.cookbook.materialdrawer.R.attr.material_drawer_background, com.cookbook.materialdrawer.R.color.material_drawer_background));
            //we do not have the MiniDrawer view during CrossfadeDrawerLayout creation so we will add it here
            crossfadeDrawerLayout.getSmallView().addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            miniResult.withCrossFader(new ICrossfader() {
                @Override
                public void crossfade() {
                    boolean isFaded = isCrossfaded();
                    crossfadeDrawerLayout.crossfade(400);

                    //only close the drawer if we were already faded and want to close it now
                    if (isFaded) {
                        result.getDrawerLayout().closeDrawer(GravityCompat.START);
                    }
                }

                @Override
                public boolean isCrossfaded() {
                    return crossfadeDrawerLayout.isCrossfaded();
                }
            });
        }
        if (savedInstanceState == null) {
            replaceFragment(mVerticalStepperAdapterDemoFragment);
        }


    }


    private void replaceFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }



    //load navigator\\
    public void loadJsonNav() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAssetNav());
            JSONArray m_jArry = obj.getJSONArray("navigator");


            for (int j = 0; j < m_jArry.length(); j++) {
                JSONObject joR_inside = m_jArry.getJSONObject(j);
                final String name = joR_inside.getString("NameCategory");
                final String nameArray = joR_inside.getString("arrayObjectsName");
                String pref = joR_inside.getString("img_prefix");
                Drawable d = Drawable.createFromStream(getApplicationContext().getAssets().open("Img_Categories/"+pref+".png"), null);
                primaryDrawerItems.add(new PrimaryDrawerItem().withName(name).withIcon(d).withIdentifier(j+1).withSelectable(true));
                if(j == 0) {
                    primaryDrawerItems.get(j).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                            cDLA.getSupportActionBar().setTitle(name);
                            cDLA.restartRandomActivity();
                            finish();
                            return false;
                        }
                    });
                }
                else {
                    primaryDrawerItems.get(j).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                        @Override
                        public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                            cDLA.arrayC = nameArray;
                            cDLA.getSupportActionBar().setTitle(name);
                            cDLA.restartActivity();
                            finish();
                            return false;
                        }
                    });
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAssetNav() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("nav.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
    //********************************************************************\\
}
