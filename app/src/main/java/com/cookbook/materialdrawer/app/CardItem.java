package com.cookbook.materialdrawer.app;


import android.graphics.drawable.Drawable;

public class CardItem {

    private String mTextResource;
    private String mTitleResource;
    private CrossfadeDrawerLayoutActvitiy cAdapter;
    private Drawable dImage;

    //private int buttonText;

    public CardItem(String title, String text, CrossfadeDrawerLayoutActvitiy miT, Drawable draw) {
        mTitleResource = title;
        mTextResource = text;
        cAdapter = miT;
        dImage = draw;
    }

    public String getText() {
        return mTextResource;
    }

    public String getTitle() {
        return mTitleResource;
    }

    public CrossfadeDrawerLayoutActvitiy getPager() {
        return cAdapter;
    }
    public Drawable getDraw() {
        return dImage;
    }


}
