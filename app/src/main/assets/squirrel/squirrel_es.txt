Paso 1) Dobla y despliega el papel por la mitad en ambos sentidos y luego dobla la parte inferior y la parte superior hacia el centro a lo largo de las lineas de puntos.


Paso 2) Dobla ambos lados hacia el centro y luego despliegalos.


Paso 3) Dobla todas las esquinas hacia abajo a lo largo de las lineas de puntos.


Paso 4) Abre cada esquina y giralas en posicion plana.


Paso 5) Empuja todos los pliegues hacia arriba.


Paso 6) Dobla el papel por detras a lo largo de las lineas de puntos.


Paso 7) Dobla las solapas a lo largo de las lineas de puntos.


Paso 8) Dobla el papel por la mitad y el fondo por detras.


Paso 9) Hacer un pliegue inverso interior a lo largo de las lineas de puntos.


Paso 10) Gira el modelo unos 90 grados.


Paso 11) Hacer un pliegue inverso exterior a lo largo de las lineas de puntos.


Paso 12) Hacer un pliegue inverso interior en la parte frontal de la cabeza.


Paso 13) Dobla el papel en el interior a lo largo de la linea de puntos y repite en el otro lado.


La ardilla completa