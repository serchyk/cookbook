Etapa 1) Dobre e desdobre o papel ao meio nas duas direcoes e depois dobre a parte inferior e a parte superior ate o centro ao longo das linhas pontilhadas.


Etapa 2) Dobre os dois lados para o centro e, em seguida, desdobre-os.


Etapa 3) Dobre todos os cantos para baixo ao longo das linhas pontilhadas.


Passo 4) Abra cada canto e gire-os.


Passo 5) Empurre todas as dobras.


Etapa 6) Dobre o papel atras das linhas pontilhadas.


Etapa 7) Dobre as abas ao longo das linhas pontilhadas.


Passo 8) Dobre o papel ao meio, trazendo o fundo para tras.


Etapa 9) Faca uma dobra reversa interna ao longo das linhas pontilhadas.


Etapa 10) Gire o modelo cerca de 90 graus.


Etapa 11) Faca uma dobra reversa externa ao longo das linhas pontilhadas.


Etapa 12) Faca uma dobra reversa interna na frente da cabeca.


Etapa 13) Dobre o papel dentro da linha pontilhada e repita do outro lado.


O esquilo completo