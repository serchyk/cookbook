Paso 1) Dobla el papel por la mitad y luego despliegalo.


Paso 2) Dobla ambos lados hacia el centro a lo largo de la linea de puntos.


Paso 3) Dobla ambos bordes a lo largo de las lineas de puntos. Realmente no importa cual haces primero.


Paso 4) Dobla todo el modelo por la mitad a lo largo de la linea de puntos.


Paso 5) Dobla el papel a lo largo de la linea de puntos.


Paso 6) Abre el papel hacia arriba y hacia la izquierda.


Paso 7) Squash Dobla el papel plano.


Paso 8) Dobla el papel por debajo a lo largo de la linea de puntos.


Paso 9) Este paso se amplia un poco en la parte de la cabeza. Haz dos pliegues detras del modelo para formar las orejas.


Paso 10) Hacer un pliegue inverso interior a lo largo de la linea de puntos.


Paso 11) Este diagrama muestra el papel dentro del modelo. Haga otro pliegue inverso interno y saque un poco de papel formando la cola.


El gatito completo