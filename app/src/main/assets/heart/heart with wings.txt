Step 1) Fold and unfold the paper in half both ways.


Step 2) Fold the top and bottom in to the centre.


Step 3) Turn the paper over.


Step 4) Fold both sides up along the dotted lines.


Step 5) Turn the paper over.


Step 6) Fold the top down and make Squash Folds on the flaps that open up. See the zoomed in part in the circle. Also take a look at the next step to see the final position of the folds.


Step 7) Fold both sides in along the dotted lines.


Step 8) Turn the paper over.


Step 9) Fold the tops in behind along the dotted lines to finish shaping the heart.


The complete heart with wings