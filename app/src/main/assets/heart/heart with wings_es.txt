Paso 1) Dobla y despliega el papel por la mitad en ambos sentidos.


Paso 2) Dobla la parte superior e inferior hacia el centro.


Paso 3) Dar la vuelta al papel.


Paso 4) Dobla ambos lados hacia arriba a lo largo de las lineas de puntos.


Paso 5) Da la vuelta al papel.


Paso 6) Dobla la parte superior hacia abajo y haz pliegues de squash en las solapas que se abren. Ver la parte ampliada en el circulo. Tambien eche un vistazo al siguiente paso para ver la posicion final de los pliegues.


Paso 7) Dobla ambos lados a lo largo de las lineas de puntos.


Paso 8) Dar la vuelta al papel.


Paso 9) Dobla las partes superiores por detras a lo largo de las lineas punteadas para terminar de dar forma al corazon.


El corazon completo con alas.