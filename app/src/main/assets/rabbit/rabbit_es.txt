Paso 1) Dobla el papel por la mitad y despliegalo.


Paso 2) Dobla ambos lados hacia el centro a lo largo de las lineas de puntos.


Paso 3) Dobla ambos lados en la parte inferior hacia el centro a lo largo de las lineas de puntos.


Paso 4) Saque el papel atrapado en el interior y doblelo para formar un pliegue de oreja de conejo en cada lado. Esto te da una base de pescado.


Paso 5) Dobla la parte superior del papel hacia abajo y detras del modelo.


Paso 6) Dobla ambos lados hacia el centro a lo largo de las lineas de puntos.


Paso 7) Doble la solapa superior del papel hacia arriba a lo largo de la linea de puntos.


Paso 8) Dobla el papel por la mitad y coloca el lado izquierdo detras del modelo.


Paso 9) Hacer un pliegue inverso interior a lo largo de la linea de puntos.


Paso 10) Haz otro pliegue inverso interior a lo largo de la linea de puntos.


Paso 11) Haz otro Pliegue inverso hacia arriba a lo largo de la linea de puntos.


Paso 12) Haz un pliegue inverso mas para que la cola no sea tan puntiaguda.


Paso 13) Gira el modelo unos 90 grados.


Paso 14) Haga un pliegue inverso exterior en la solapa media del papel.


Paso 15) Haz otro pliegue inverso interior para completar la cabeza.


Paso 16) Abre ambos oidos haciendolos un poco mas 3D.


El conejo completo