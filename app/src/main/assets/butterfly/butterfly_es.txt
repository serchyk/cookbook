Paso 1) Comience con una hoja cuadrada de papel con el lado blanco hacia arriba. Doblarlo por la mitad horizontalmente, arrugarlo bien y luego desplegarlo.


Paso 2) Dobla el papel por la mitad verticalmente, pliegalo bien y luego despliegalo.


Paso 3) Dobla el cuarto superior del papel a lo largo de la linea de puntos hasta la linea central.


Paso 4) Crea bien este pliegue y luego despliegalo.


Paso 5) Dobla el cuarto inferior del papel a lo largo de la linea de puntos hasta la linea central.


Paso 6) Crea bien este pliegue y luego despliegalo.


Paso 7) Dobla el cuarto derecho del papel a lo largo de la linea de puntos hasta la linea central.


Paso 8) Crea bien este pliegue y luego despliegalo.


Paso 9) Dobla el cuarto izquierdo del papel a lo largo de la linea de puntos hasta la linea central.


Paso 10) Crea bien este pliegue y luego despliegalo.


Paso 11) Dobla el papel por la mitad en diagonal. Creelo bien y luego despliegalo.


Paso 12) Dobla el papel por la mitad en diagonal hacia el otro lado. Creelo bien y luego despliegalo.


Paso 13) Gire el papel de manera que el lado coloreado quede hacia arriba.


Paso 14) Dobla la esquina del papel hacia el centro a lo largo de la linea de puntos. Crea bien este pliegue y luego despliegalo.


Paso 15) Dobla la siguiente esquina del papel hacia el centro a lo largo de la linea de puntos. Crea bien este pliegue y luego despliegalo.


Paso 16) Dobla la siguiente esquina del papel hacia el centro a lo largo de la linea de puntos. Crea bien este pliegue y luego despliegalo.


Paso 17) Dobla la esquina final del papel hacia el centro a lo largo de la linea de puntos. Crea bien este pliegue y luego despliegalo.


Paso 18) Gire el papel para que el lado blanco quede hacia arriba nuevamente.


Paso 19) Dobla los dos lados del papel hacia el centro a lo largo de la linea de puntos. Ya tienes un pliegue aqui. Solo doble la mitad media del papel donde estan las lineas de puntos. No doble las partes superior o inferior del pliegue existente.


Paso 20) Debes tener una forma que se parezca a lo que ves aqui. Dobla la parte superior e inferior del modelo a lo largo de las lineas de puntos hacia el centro.


Paso 21) Esta es una Base de barco completada. Dobla las dos solapas inferiores de papel a lo largo de las lineas de puntos.


Paso 22) Da la vuelta al modelo.


Paso 23) Dobla la parte superior del papel hacia abajo a lo largo de la linea de puntos.


Paso 24) Da la vuelta al modelo otra vez.


Paso 25) Dobla la solapa superior del papel a la derecha a lo largo de la linea de puntos hacia el centro. La linea de puntos va desde la esquina superior hasta la mitad de la linea diagonal en la parte inferior, como se puede ver en el diagrama.


Paso 26) Dobla la solapa superior de papel a la izquierda a lo largo de la linea de puntos hacia el centro. La linea de puntos va desde la esquina superior hasta la mitad de la linea diagonal en la parte inferior, como se puede ver en el diagrama.


Paso 27) Da la vuelta al modelo.


Paso 28) Pliegue en valle a lo largo de la linea de puntos en la direccion de la flecha en el diagrama. Crea bien este pliegue y luego despliegalo.


Paso 29) Pliegue en el valle a lo largo de la linea de puntos en el otro lado en la direccion de la flecha en el diagrama. Crea bien este pliegue y luego despliegalo.


Paso 30) Aprieta el centro del modelo a lo largo de los dos pliegues que acabas de hacer y forma un Pliegue de Montana a lo largo de la linea vertical del centro. Esto le da a la mariposa su forma final.


La mariposa tradicional completada de Origami.